## quakejs
This is a Dockerfile that provides a container image that runs [quakejs](https://github.com/inolen/quakejs)

### Usage
**1**. Clone the docker repo (with --recurse-submodules)
```
[jlay@heimdall tmp]$ git clone --recurse-submodules https://gitlab.com/jlay/docker.git
Cloning into 'docker'...
remote: Enumerating objects: 12, done.
remote: Counting objects: 100% (12/12), done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 12 (delta 1), reused 0 (delta 0)
Unpacking objects: 100% (12/12), done.
Submodule 'quakejs/quakejs-git' (https://github.com/inolen/quakejs.git) registered for path 'quakejs/quakejs-git'
Cloning into 'quakejs/quakejs-git'...
remote: Enumerating objects: 177, done.
remote: Total 177 (delta 0), reused 0 (delta 0), pack-reused 177
Receiving objects: 100% (177/177), 1.86 MiB | 0 bytes/s, done.
Resolving deltas: 100% (101/101), done.
Submodule path 'quakejs/quakejs-git': checked out '5660b0b5131023dff03eefd8fa5b07546af2838d'
Submodule 'ioq3' (git://github.com/inolen/ioq3.git) registered for path 'ioq3'
Cloning into 'ioq3'...
remote: Enumerating objects: 1, done.
remote: Counting objects: 100% (1/1), done.
remote: Total 19134 (delta 0), reused 0 (delta 0), pack-reused 19133
Receiving objects: 100% (19134/19134), 23.03 MiB | 24.26 MiB/s, done.
Resolving deltas: 100% (14196/14196), done.
Submodule path 'quakejs/quakejs-git/ioq3': checked out '4f7d7bf2159aa0a18b79bb417aa760abac817b2a'
[jlay@heimdall tmp]$
```

**2**. Build the image
```
[jlay@heimdall tmp]$ cd docker/quakejs/
[jlay@heimdall quakejs]$ docker build -t quakejs .
Sending build context to Docker daemon 31.15 MB
Step 1/7 : FROM node:8-alpine
[...]
Successfully built 69eec0f5f667
[jlay@heimdall quakejs]$
```

**3**. Run the container
```
[jlay@heimdall quakejs]$ docker run -dt --name quakejs -p 8080:8080 quakejs
8c78561048641349fecb0d795c250201842285ec1d415f1d92ee40c14d662b2b
[jlay@heimdall quakejs]$ docker ps
CONTAINER ID        IMAGE                                COMMAND                  CREATED             STATUS              PORTS                                                                                                   NAMES
[...]
8c7856104864        quakejs                              "node bin/web.js -..."   2 seconds ago       Up 1 second         0.0.0.0:8080->8080/tcp                                                                                  quakejs
[jlay@heimdall quakejs]$
```

**4**. Play!  Access the host at `http://$IP:8080` to reach quakejs
